# csdl_test

load and parse a tiled map    
build collision map    
build and render map    
move map and collision layer     
player sprite animation    
handle collisions with player and map    
      
press 'f' for fullscreen    
press 'd' to render collision map    
press 'esc' to quit    
      
<img src="https://github.com/nsklaus/csdl_test/blob/main/screenshots/screen1.png?raw=true" width="250" > <img src="https://github.com/nsklaus/csdl_test/blob/main/screenshots/screen2.png?raw=true" width="250">
